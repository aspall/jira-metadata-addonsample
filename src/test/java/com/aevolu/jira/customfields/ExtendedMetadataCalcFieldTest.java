package com.aevolu.jira.customfields;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.aevolu.jira.customfields.ExtendedMetadataCalcFieldType;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class ExtendedMetadataCalcFieldTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //ExtendedMetadataCalcField testClass = new ExtendedMetadataCalcField();

        throw new Exception("ExtendedMetadataCalcField has no tests!");

    }

}
