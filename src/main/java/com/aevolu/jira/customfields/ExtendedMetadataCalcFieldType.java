package com.aevolu.jira.customfields;

import java.util.List;
import java.util.Map;

import org.apache.velocity.tools.generic.MathTool;

import com.atlassian.core.util.XMLUtils;
import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.config.item.DefaultValueConfigItem;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.searchers.ExactNumberSearcher;
import com.atlassian.jira.issue.customfields.searchers.NumberRangeSearcher;
import com.atlassian.jira.issue.customfields.searchers.TextSearcher;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.velocity.NumberTool;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.opensymphony.util.TextUtils;
import com.osoboo.jira.metadata.CustomFieldLookupService;
import com.osoboo.jira.metadata.MetadataService;

public class ExtendedMetadataCalcFieldType extends CalculatedCFType {

	private final WebFragmentHelper webFragmentHelper;
	private MetadataService metadataService;
	private CustomFieldLookupService customFieldLookupService;
	private CustomFieldValuePersister customFieldValuePersister;
	private GenericConfigManager genericConfigManager;


	protected ExtendedMetadataCalcFieldType(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager,
			JiraAuthenticationContext authenticationContext, WebFragmentHelper webFragmentHelper, MetadataService metadataService,
			CustomFieldLookupService customFieldLookupService) {
		this.customFieldValuePersister = customFieldValuePersister;
		this.genericConfigManager = genericConfigManager;
		this.webFragmentHelper = webFragmentHelper;
		this.metadataService = metadataService;
		this.customFieldLookupService = customFieldLookupService;
	}

	@Override
	public Object getValueFromIssue(CustomField customField, Issue issue) {
		try {
			final Map<String, Object> startingParams = JiraVelocityUtils.getDefaultVelocityParams(ComponentManager.getInstance()
					.getJiraAuthenticationContext());
			startingParams.put("issue", issue);
			startingParams.put("customField", customField);
			startingParams.put("metadataService", metadataService);
			startingParams.put("math", new MathTool());
			startingParams.put("numberTool", new NumberTool(getI18nBean().getLocale()));
			startingParams.put("textutils", new TextUtils());
			startingParams.put("customFieldLookupService", customFieldLookupService);
			startingParams.put("componentManager", ComponentManager.getInstance());
			String calcMetadataValue = webFragmentHelper.renderVelocityFragment(getDefaultValue(customField.getRelevantConfig(issue))
					.toString(), startingParams);
			if (customField.getCustomFieldSearcher() instanceof TextSearcher) {
				return calcMetadataValue;
			}
			try {
				return Double.parseDouble(calcMetadataValue);
			} catch (NumberFormatException e) {
				if (customField.getCustomFieldSearcher() instanceof ExactNumberSearcher
						|| customField.getCustomFieldSearcher() instanceof NumberRangeSearcher) {
					// TODO you should add logging
					return Double.valueOf(0.0);
				}
			}
			return calcMetadataValue;
		} catch (Exception e) {
			if (customField.getCustomFieldSearcher() instanceof ExactNumberSearcher
					|| customField.getCustomFieldSearcher() instanceof NumberRangeSearcher) {
				return Double.valueOf(0.0);
			}
			return "";
		}
	}
	
	/**
	 * checks whether the given values can be transformed to Double (and
	 * compared) or calls super.compare.
	 */
	@Override
	public int compare(Object value1, Object value2, FieldConfig fieldConfig) {
		try {
			Double double1 = Double.valueOf(value1.toString());
			return double1.compareTo(Double.valueOf(value2.toString()));
		} catch (RuntimeException e) {
			return super.compare(value1, value2, fieldConfig);
		}
	}

	@Override
	public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
		Map<String, Object> values = super.getVelocityParameters(issue, field, fieldLayoutItem);
		if (!values.containsKey("numberTool")) { //$NON-NLS-1$
			values.put("numberTool", new NumberTool(getI18nBean().getLocale())); //$NON-NLS-1$
		}
		if (!values.containsKey("textutils")) { //$NON-NLS-1$
			values.put("textutils", new TextUtils()); //$NON-NLS-1$
		}
		if (!values.containsKey("xmlutils")) { //$NON-NLS-1$
			values.put("xmlutils", new XMLUtils()); //$NON-NLS-1$
		}
		return values;
	}

	@Override
	public Object getSingularObjectFromString(String arg0) throws FieldValidationException {
		return arg0;
	}

	@Override
	public String getStringFromSingularObject(Object arg0) {
		if (arg0 == null) {
			return ""; //$NON-NLS-1$
		}
		return arg0.toString();
	}

	@Override
	public List getConfigurationItemTypes() {
		return CollectionBuilder.<FieldConfigItemType> newBuilder(JiraUtils.loadComponent(DefaultValueConfigItem.class)).asArrayList();
	}

	public void createValue(CustomField field, Issue issue, Object value) {
		customFieldValuePersister.createValues(field, issue.getId(), getDatabaseType(), EasyList.build(getDbValueFromObject(value)));
	}

	protected PersistenceFieldType getDatabaseType() {
		return PersistenceFieldType.TYPE_LIMITED_TEXT;
	}

	/**
	 * the value does exist, and the new value is different than the existing
	 * one.
	 */
	public void updateValue(CustomField customField, Issue issue, Object value) {
		customFieldValuePersister.updateValues(customField, issue.getId(), getDatabaseType(), EasyList.build(getDbValueFromObject(value)));
	}

	private Object getDbValueFromObject(Object value) {
		return value;
	}

	public void setDefaultValue(FieldConfig fieldConfig, Object value) {
		genericConfigManager.update(DEFAULT_VALUE_TYPE, fieldConfig.getId().toString(), getDbValueFromObject(value));
	}

	public Object getDefaultValue(FieldConfig fieldConfig) {
		try {
			Object databaseValue = genericConfigManager.retrieve(DEFAULT_VALUE_TYPE, fieldConfig.getId().toString());
			if (databaseValue != null) {
				return getObjectFromDbValue(databaseValue);
			} else {
				return null;
			}
		} catch (FieldValidationException e) {
			return null;
		}
	}

	private Object getObjectFromDbValue(Object databaseValue) {
		return databaseValue;
	}
}